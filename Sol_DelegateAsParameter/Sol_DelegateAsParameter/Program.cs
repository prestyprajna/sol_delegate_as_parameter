﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_DelegateAsParameter
{
    class Program
    {
        static void Main(string[] args)
        {
            //without  delegate 
            //int result=new Calculation().Add(2, 3);
            //Console.WriteLine(result);

            //int result1 = new Calculation().Multiply(2, 3);
            //Console.WriteLine(result1);


            //with delegate
            int result=new Calculation().Cal(2, 3, (leVal1, leVal2) =>
              {
                  return leVal1 + leVal2;
              });
            Console.WriteLine(result);

            result = new Calculation().Cal(2, 3, (leVal1, leVal2) =>
                {
                    return leVal1 * leVal2;
                });
            Console.WriteLine(result);

        }
    }

    public delegate int CalculationDelegate(int val1, int val2);

    public class Calculation
    {
        //public int Add(int val1,int val2)
        //{
        //    return (val1 + val2)*5;
        //}

        //public int Multiply(int val1,int val2)
        //{
        //    return (val1 * val2) * 5;
        //}

        public int Cal(int val1,int val2,CalculationDelegate calDelObj)
        {
            return calDelObj(val1, val2) * 5;
        }
    }

        
}
